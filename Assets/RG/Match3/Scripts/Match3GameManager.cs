using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class Match3GameManager : Singleton<Match3GameManager> {
    const float GRAVITY = 9.81f;
    private const float TILESIZE = 1f;
    [SerializeField]
    Transform gridOrigin;
    [SerializeField]
    private int gridRows;
    [SerializeField]
    private int gridColumns;
    [SerializeField]
    private Tile[] tilesPrefabs;
    private Tile[,] tilesGrid;
    private List<Tile> justLandedTiles = new List<Tile>();
    private int fallingColumns = 0;
    public bool AreTilesFalling { get => fallingColumns > 0; }

    void Start() {
        SpawnGrid();
    }

    private void SpawnGrid() {
        if (tilesPrefabs.Length < 2) {
            Debug.LogError("There are not enough tiles prefabs");
            return;
        }

        Assert.AreNotEqual(0, gridRows);
        Assert.AreNotEqual(0, gridColumns);
        tilesGrid = new Tile[gridRows, gridColumns];
        for (int i = 0; i < gridRows; i++) {
            int sameTileTypeCounter = 0;
            int previousTilesPrefabIndex = -1;
            for (int j = 0; j < gridColumns; j++) {
                Vector3 tilePosition = gridOrigin.position + gridOrigin.up * TILESIZE * i + gridOrigin.right * TILESIZE * j;
                int randomTilesPrefabIndex = UnityEngine.Random.Range(0, tilesPrefabs.Length);
                if (randomTilesPrefabIndex == previousTilesPrefabIndex) {
                    sameTileTypeCounter++;
                    if (sameTileTypeCounter == 2) {
                        randomTilesPrefabIndex = (randomTilesPrefabIndex + 1) % tilesPrefabs.Length;
                        sameTileTypeCounter = 0;
                    }
                }
                else {
                    sameTileTypeCounter = 0;
                }

                previousTilesPrefabIndex = randomTilesPrefabIndex;
                Tile tile = Instantiate(tilesPrefabs[randomTilesPrefabIndex], tilePosition, gridOrigin.rotation, transform);
                tilesGrid[i, j] = tile;
                tile.DestinationRowIndex = i;
                tile.ColumnIndex = j;
                tile.PrefabType = randomTilesPrefabIndex;
                tile.Clicked += OnTileClicked;
            }
        }
    }

    private void RemoveTilesAndInitiateFall(Tile[] tiles) {
        for (int i = 0; i < tiles.Length; i++) {
            Tile tile = tiles[i];
            tilesGrid[tile.DestinationRowIndex, tile.ColumnIndex] = null;
            tile.gameObject.SetActive(false);
        }

        ///We order column piece fall coroutines start by the lower tile row
        ///so that the column pieces fall on each other in a proper way being aware if there are just landed tiles below 
        Array.Sort(tiles);

        for (int i = 0; i < tiles.Length; i++) {
            List<Tile> fallingTiles = new List<Tile>();
            Tile tile = tiles[i];
            for (int j = tile.DestinationRowIndex + 1; j < gridRows; j++) {
                Tile fallingtile = tilesGrid[j, tile.ColumnIndex];
                if (fallingtile != null) {
                    fallingTiles.Add(fallingtile);
                    tilesGrid[j, tile.ColumnIndex] = null;
                    int newRow = j - 1;
                    fallingtile.DestinationRowIndex = newRow;
                }
                else {
                    break;
                }
            }

            if (fallingTiles.Count != 0) {
                fallingColumns++;
                StartCoroutine(TilesColumnPieceFallCoroutine(fallingTiles.ToArray()));
            }
        }
    }
    private void OnTileClicked(Tile tile) {
        RemoveTilesAndInitiateFall(new Tile[] { tile });
    }

    private IEnumerator TilesColumnPieceFallCoroutine(Tile[] fallingTiles) {
        float speed = 0;
        float fallLength = 0;
        float fallDistance = TILESIZE;

        while (fallLength < fallDistance) {
            speed += (GRAVITY * Time.deltaTime);
            float fallDelta = speed * Time.deltaTime;
            fallLength += fallDelta;
            if (fallLength >= fallDistance) {

                ///In case the grid is very tall terminal speed of column pieces can grow and framerate can drop enough to make a tile 2+ rows in a frame
                ///so we check where it should be and if it can keep falling, another solution to deal with this problem is to set a speedlimit but it's less ideal
                int fallTileLengthRatio = Mathf.FloorToInt((fallLength - fallDistance) / TILESIZE);
                Tile lowerTile = fallingTiles[0];
                bool mustStop = false;
                int destinationOffset = 0;
                for (int i = 0; i <= fallTileLengthRatio; i++) {
                    mustStop |= ((lowerTile.DestinationRowIndex - i) <= 0 ||
                    tilesGrid[lowerTile.DestinationRowIndex - (1 + i), lowerTile.ColumnIndex] != null);
                    if (mustStop) {
                        destinationOffset = i;
                        mustStop = true;
                        break;
                    }
                }

                if (mustStop) {
                    for (int i = 0; i < fallingTiles.Length; i++) {
                        Tile tile = fallingTiles[i];
                        tile.DestinationRowIndex -= destinationOffset;
                        tile.transform.position = gridOrigin.position + gridOrigin.up * TILESIZE * tile.DestinationRowIndex + gridOrigin.right * TILESIZE * tile.ColumnIndex;
                        tilesGrid[tile.DestinationRowIndex, tile.ColumnIndex] = tile;
                    }
                    break;
                }
                else {
                    fallDistance += TILESIZE * (1 + fallTileLengthRatio);
                    for (int i = 0; i < fallingTiles.Length; i++) {
                        fallingTiles[i].DestinationRowIndex -= (1 + fallTileLengthRatio);
                    }
                }
            }

            for (int i = 0; i < fallingTiles.Length; i++) {
                fallingTiles[i].transform.position -= gridOrigin.up * fallDelta;
            }
            yield return null;

        }

        justLandedTiles.AddRange(fallingTiles);
        fallingColumns--;
    }

    private void LateUpdate() {
        if (justLandedTiles.Count != 0 && !AreTilesFalling) {
            List<Tile> tilesToRemoveList = new List<Tile>();
            for (int i = 0; i < justLandedTiles.Count; i++) {
                Tile tile = justLandedTiles[i];
                if (!tilesToRemoveList.Contains(tile)) {
                    Tile[] removableTiles = GetRemovableTilesRow(tile);
                    tilesToRemoveList.AddRange(removableTiles);
                }
            }

            RemoveTilesAndInitiateFall(tilesToRemoveList.ToArray());
            justLandedTiles.Clear();
        }
    }

    private Tile[] GetRemovableTilesRow(Tile tile) {
        List<Tile> tilesToRemove = new List<Tile>() { tile };
        int nearColumnIndex = tile.ColumnIndex + 1;
        while (nearColumnIndex < gridColumns) {
            Tile nearTile = tilesGrid[tile.DestinationRowIndex, nearColumnIndex];
            if (nearTile != null && tile.PrefabType == nearTile.PrefabType) {
                tilesToRemove.Add(nearTile);
            }
            else {
                break;
            }
            nearColumnIndex++;
        }

        nearColumnIndex = tile.ColumnIndex - 1;
        while (nearColumnIndex >= 0) {
            Tile nearTile = tilesGrid[tile.DestinationRowIndex, nearColumnIndex];
            if (nearTile != null && tile.PrefabType == nearTile.PrefabType) {
                tilesToRemove.Add(nearTile);
            }
            else {
                break;
            }

            nearColumnIndex--;
        }

        if (tilesToRemove.Count >= 3) {
            return tilesToRemove.ToArray();
        }
        else {
            return new Tile[0];
        }
    }
}