using System;
using UnityEngine;

public class Tile : MonoBehaviour, IComparable<Tile> {
    public Action<Tile> Clicked;
    int columnIndex;
    int destinationRowIndex;
    int prefabType;

    public int ColumnIndex { get => columnIndex; set => columnIndex = value; }
    public int DestinationRowIndex {
        get => destinationRowIndex;
        set => destinationRowIndex = value;
    }
    public int PrefabType { get => prefabType; set => prefabType = value; }
    public void Click() {
        Clicked?.Invoke(this);
    }

    public int CompareTo(Tile other) {
        if (this.destinationRowIndex > other.destinationRowIndex) {
            return 1;
        }
        if (this.destinationRowIndex < other.destinationRowIndex) {
            return -1;
        }

        return 0;
    }
}
