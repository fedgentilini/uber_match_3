﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Match3InputController : Singleton<Match3InputController> {
    const string TILELAYERNAME = "Tile";

    void Update() {
        if (Input.GetMouseButtonDown(0) && !Match3GameManager.Instance.AreTilesFalling) {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 100.0f, LayerMask.GetMask(TILELAYERNAME))) {
                Tile tile = hit.collider.GetComponent<Tile>();
                if (tile != null) {
                    tile.Click();
                }
            }
        }
    }
}
